'use strict';
$(document).ready(function() {
    setTimeout(function() {

    // [ line-smooth-chart ] start
    Morris.Line({
        element: 'programme-chart',
        data: [{
                m: '2020-01',
                a: 0,
                b: 30,
                c: 0
            },
            {
                m: '2020-02',
                a: 0,
                b: 20,
                c: 0
            },
            {
                m: '2020-03',
                a: 30,
                b: 10,
                c: 5
            },
            {
                m: '2020-04',
                a: 20,
                b: 30,
                c: 45
            },
            {
                m: '2020-05',
                a: 20,
                b: 35,
                c: 60
            },
            {
                m: '2020-06',
                a: 0,
                b: 40,
                c: 50
            },
            {
                m: '2020-07',
                a: 0,
                b: 35,
                c: 45
            },
            {
                m: '2020-08',
                a: 0,
                b: 32,
                c: 24
            },
            {
                m: '2020-09',
                a: 0,
                b: 28,
                c: 32
            },
            {
                m: '2020-10',
                a: 0,
                b: 30,
                c: 18
            },
            {
                m: '2020-11',
                a: 0,
                b: 24,
                c: 15
            },
            {
                m: '2020-12',
                a: 0,
                b: 30,
                c: 20
            },
            {
                m: '2021-01',
                a: 0,
                b: 38,
                c: 29
            },
            {
                m: '2021-02',
                a: 0,
                b: 32,
                c: 23
            },
            {
                m: '2021-03',
                a: 0,
                b: 40,
                c: 15
            }
        ],
        xkey: 'm',
        redraw: true,
        resize: true,
        ykeys: ['a','b','c'],
        hideHover: 'auto',
        responsive:true,
        labels: ['SJPP', 'Bizpower', 'SRF'],
        lineColors: ['#1de9b6', '#A389D4','#EF3E42']
    });
    // [ line-smooth-chart ] end
        }, 400);
});
